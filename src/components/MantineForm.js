import { 
    Box, 
    Button, 
    FileInput,
    LoadingOverlay, 
    Modal, 
    PasswordInput, 
    TextInput } 
from "@mantine/core";

import { useForm } from "@mantine/form";
import { useDisclosure } from "@mantine/hooks";
import { useState } from "react";

function MantineForm() {
    
    const [opened, {open, close}] = useDisclosure(false);
    
    const [visible, setVisible] = useState(false);
    const [message, setMessage] = useState('');
    const [file, setFile] = useState(null);

    function isNameValid(value) {
        var validName = /^[a-z ,.'-]+$/i;

        if (value.length === 0)
            return 'name should not be empty';

        if (!validName.test(value))
            return 'name should not contain special characters';

        return '';
    }

    function isEmailValid(value) {
        if (value.length === 0)
            return 'Email address should not be empty';

        if (value.includes('@'))
            return '';
        
        return 'Invalid email address';
    }

    function submitForm() {
        setVisible(true);
        fetch(
            'https://mantienformtest-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
            {
              method: 'POST',
              body: JSON.stringify(form.values),
              headers: {
                'Content-Type': 'application/json'
              }
            }
          ).then(() => {
            fetch(
                'https://firebasestorage.googleapis.com/v0/b/mantienformtest.appspot.com/o/stash%2F'+ file.name,
                {
                    method: 'POST',
                    body: file,
                    headers: {
                        'Content-Type': 'image/png'
                    }
                }
            ).then(() => {
                form.reset();
                setFile(null);
                setMessage("Succesfully submitted");
                setVisible(false);
                open();
            }).catch(() => {
                setMessage("Submition failed");
                setVisible(false);
                open();
    
            })
          })
          .catch((e) => {
            setMessage("Submition failed");
            setVisible(false);
            open();
          });
    }

    const form = useForm({
        initialValues: {
            user : {
                firstName : '',
                lastName : '',
            },

            email : '',

            password : {
                initialPassword : '',
                confirmPassword : ''
            },
        },

        validate : {
            user : {
                firstName : (value) => (isNameValid(value).length !== 0 ? "First " + isNameValid(value) : null),
                lastName : (value) => (isNameValid(value).length !== 0 ? "Last " + isNameValid(value) : null),
            },

            email : (value) => (isEmailValid(value).length !== 0 ? isEmailValid(value) : null),

            password : {
                initialPassword : (value) => (value.length === 0 ? 'Password should not be empty' : null),
                confirmPassword : (value, values) => (value !== values.password.initialPassword ? 'Password does not match the repeat' : null)
            }
        },

        validateInputOnChange : true
    });

    return(
        <Box maw={320} mx="auto">
            <LoadingOverlay visible={visible} overlayBlur={2} />

            <Modal opened={opened} onClose={close} title="Attention"> 
                {message} 
            </Modal>

            <form onSubmit={form.onSubmit(submitForm)}>
                <TextInput 
                    label="First Name" 
                    placeholder="John" 
                    mt='md'
                    withAsterisk
                    {...form.getInputProps('user.firstName')} 
                />

                <TextInput 
                    label="Last Name" 
                    placeholder="Smith" 
                    mt='md'
                    withAsterisk
                    {...form.getInputProps('user.lastName')} 
                />

                <TextInput
                    label="Email"
                    placeholder="your@gmail.com"
                    mt='md'
                    withAsterisk
                    {...form.getInputProps('email')}
                />

                <FileInput
                    value={file}
                    onChange={setFile}
                    placeholder="Picke a photo"
                    mt='md'
                    accept="image/png, image/jpeg"
                    label="Profile Photo"
                />

                <PasswordInput
                    label="Password"
                    placeholder="password"
                    mt='md'
                    withAsterisk
                    {...form.getInputProps('password.initialPassword')}
                />

                <PasswordInput
                    label="Confirm Password"
                    placeholder="repeat password"
                    mt='md'
                    {...form.getInputProps('password.confirmPassword')}
                />

                <Button type="submit" mt="sm">
                    Submit
                </Button>
            </form>
        </Box>

    )
}

export default MantineForm;