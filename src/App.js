import logo from './logo.svg';
import './App.css';
import MantineForm from './components/MantineForm';

function App() {
  return (
    <div>
      <MantineForm/>
    </div>
  );
}

export default App;
